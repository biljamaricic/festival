INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
              
INSERT INTO `festivali`.`mesto` (`id`, `drzava`, `grad`) VALUES ('1', 'SRB', 'Novi Sad');
INSERT INTO `festivali`.`mesto` (`id`, `drzava`, `grad`) VALUES ('2', 'HUN', 'Budapest');
INSERT INTO `festivali`.`mesto` (`id`, `drzava`, `grad`) VALUES ('3', 'GRE', 'Thessaloniki');
INSERT INTO `festivali`.`mesto` (`id`, `drzava`, `grad`) VALUES ('4', 'MKD', 'Skopje');

INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('1', '2000', '800', '2022-11-25', 'Neki festival 2', '2022-11-22', '1');
INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('2', '3000', '20', '2021-11-25', 'Neki festival 1', '2021-11-22', '1');
INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('3', '1400', '5', '2022-03-29', 'Fest fest', '2022-03-22', '2');
INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('4', '1500', '1000', '2020-11-25', 'Stfe', '2020-11-22', '2');
INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('5', '7000', '300', '2021-12-25', 'Mimoza doza', '2022-12-22', '3');
INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('6', '8300', '50', '2022-11-25', 'Ruska salatica fest', '2022-11-22', '4');
INSERT INTO `festivali`.`festival` (`id`, `cena`, `dostupne_karte`, `kraj`, `naziv`, `pocetak`, `mesto_id`) VALUES ('7', '3700', '20', '2022-11-25', 'Kobasicijada', '2022-11-22', '4');

INSERT INTO `festivali`.`rezervacija` (`id`, `broj_karata`, `ukupna_cena`, `festival_id`) VALUES ('1', '3', '6000', '1');
INSERT INTO `festivali`.`rezervacija` (`id`, `broj_karata`, `ukupna_cena`, `festival_id`) VALUES ('2', '5', '15000', '2');
