package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Festival;
import jwd56.test.model.Rezervacija;
import jwd56.test.web.dto.FestivalDTO;

@Component
public class FestivalToFestivalDto implements Converter<Festival, FestivalDTO> {

	@Override
	public FestivalDTO convert(Festival source) {
		FestivalDTO dto = new FestivalDTO();
		
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setPocetak(source.getPocetak());
		dto.setKraj(source.getKraj());
		dto.setCena(source.getCena());
		dto.setDostupneKarte(source.getDostupneKarte());
		dto.setMestoId(source.getMesto().getId());
		
		if (source.getRezervacije() != null) {
			Integer sveRezervacije = source.getRezervacije().stream().map(Rezervacija::getBrojKarata).collect(Collectors.summingInt(Integer::intValue));
			Integer preostaleKarte = source.getDostupneKarte() - sveRezervacije;		
			dto.setPreostaleKarte(preostaleKarte);
		} else {
			dto.setPreostaleKarte(source.getDostupneKarte());
		}
		
		return dto;
	}
	
	public List<FestivalDTO> convert(List<Festival> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
