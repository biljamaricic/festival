package jwd56.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Festival;
import jwd56.test.model.Rezervacija;
import jwd56.test.service.FestivalService;
import jwd56.test.service.RezervacijaService;
import jwd56.test.web.dto.RezervacijaDTO;

@Component
public class RezervacijaDtoToRezervacija implements Converter<RezervacijaDTO, Rezervacija> {

	@Autowired
	private RezervacijaService service;
	
	@Autowired
	private FestivalService festivalService;
	
	@Override
	public Rezervacija convert(RezervacijaDTO dto) {
		
		Rezervacija entitet;
		
		if (dto.getId() == null) {
			entitet = new Rezervacija();
		} else {
			entitet = service.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setBrojKarata(dto.getBrojKarata());
			
			Festival festival = festivalService.findOne(dto.getFestivalId());
			entitet.setFestival(festival);
			
			Double ukupnaCena = dto.getBrojKarata() * festival.getCena();
			entitet.setUkupnaCena(ukupnaCena);

		}
		return entitet;
	}

}