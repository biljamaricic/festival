package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Rezervacija;
import jwd56.test.web.dto.RezervacijaDTO;

@Component
public class RezervacijaToRezervacijaDto implements Converter<Rezervacija, RezervacijaDTO> {

	@Override
	public RezervacijaDTO convert(Rezervacija source) {
		RezervacijaDTO dto = new RezervacijaDTO();
		
		dto.setId(source.getId());
		dto.setBrojKarata(source.getBrojKarata());
		dto.setUkupnaCena(source.getUkupnaCena());
		dto.setFestivalId(source.getFestival().getId());
		
		return dto;
	}
	
	public List<RezervacijaDTO> convert(List<Rezervacija> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
