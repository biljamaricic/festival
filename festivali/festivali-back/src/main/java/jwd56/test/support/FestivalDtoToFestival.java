package jwd56.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Festival;
import jwd56.test.model.Mesto;
import jwd56.test.service.FestivalService;
import jwd56.test.service.MestoService;
import jwd56.test.web.dto.FestivalDTO;

@Component
public class FestivalDtoToFestival implements Converter<FestivalDTO, Festival> {

	@Autowired
	private FestivalService service;
	
	@Autowired
	private MestoService mestoService;
	
	@Override
	public Festival convert(FestivalDTO dto) {
		
		Festival entitet;
		
		if (dto.getId() == null) {
			entitet = new Festival();
		} else {
			entitet = service.findOne(dto.getId());
		}
		
		if (entitet != null) {
			entitet.setNaziv(dto.getNaziv());
			entitet.setPocetak(dto.getPocetak());
			entitet.setKraj(dto.getKraj());
			entitet.setCena(dto.getCena());
			entitet.setDostupneKarte(dto.getDostupneKarte());
			
			Mesto mesto = mestoService.findOne(dto.getMestoId());
			entitet.setMesto(mesto);
		}
		return entitet;
	}
}