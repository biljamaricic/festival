package jwd56.test.support;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd56.test.model.Mesto;
import jwd56.test.web.dto.MestoDTO;

@Component
public class MestoToMestoDto implements Converter<Mesto, MestoDTO> {

	@Override
	public MestoDTO convert(Mesto source) {
		MestoDTO dto = new MestoDTO();
		
		dto.setId(source.getId());
		dto.setGrad(source.getGrad());
		dto.setDrzava(source.getDrzava());

		return dto;
	}
	
	public List<MestoDTO> convert(List<Mesto> lista){
		return lista.stream().map(this::convert).collect(Collectors.toList());
	}
}
