package jwd56.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jwd56.test.model.Mesto;

@Repository
public interface MestoRepository extends JpaRepository<Mesto, Long> {
	
	Mesto findOneById(Long id);

}