package jwd56.test.service;

import java.util.List;

import jwd56.test.model.Mesto;

public interface MestoService {

	Mesto findOne(Long id);
	
	List<Mesto> findAll();

}
