package jwd56.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd56.test.model.Festival;
import jwd56.test.repository.FestivalRepository;
import jwd56.test.service.FestivalService;

@Service
public class JpaFestivalService implements FestivalService {
	
	@Autowired
	private FestivalRepository repository;

	@Override
	public Festival findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Festival> findAll() {
		return repository.findAll();
	}

	@Override
	public Festival save(Festival entitet) {
		return repository.save(entitet);
	}

	@Override
	public Festival update(Festival entitet) {
		return repository.save(entitet);
	}

	@Override
	public Festival delete(Long id) {
		Optional<Festival> opt = repository.findById(id);
		if (opt.isPresent()) {
			repository.deleteById(id);
			return opt.get();
		}
		return null;
	}

	@Override
	public Page<Festival> search(String naziv, Long mestoId, int pageNo) {
		
		if (naziv == null) {
			naziv = "";
		}
		
		if (mestoId == null) {
			return repository.findByNazivIgnoreCaseContains(naziv, PageRequest.of(pageNo, 3));
		} else {
			return repository.findByNazivIgnoreCaseContainsAndMestoId(naziv, mestoId, PageRequest.of(pageNo, 3));
		}
	}
}
