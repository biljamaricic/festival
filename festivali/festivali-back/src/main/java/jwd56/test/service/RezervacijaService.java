package jwd56.test.service;

import java.util.List;

import jwd56.test.model.Rezervacija;

public interface RezervacijaService {

	Rezervacija findOne(Long id);
	
	List<Rezervacija> findAll();

	Rezervacija save(Rezervacija entitet);
}
