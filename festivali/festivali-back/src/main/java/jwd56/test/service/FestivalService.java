package jwd56.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import jwd56.test.model.Festival;

public interface FestivalService {

	Festival findOne(Long id);

	List<Festival> findAll();

	Page<Festival> search(String naziv, Long mestoId, int pageNo);
	
	Festival save(Festival entitet);

	Festival update(Festival entitet);
	
	Festival delete(Long id);

}
