package jwd56.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd56.test.model.Mesto;
import jwd56.test.repository.MestoRepository;
import jwd56.test.service.MestoService;

@Service
public class JpaMestoService implements MestoService {
	
	@Autowired
	private MestoRepository repository;

	@Override
	public Mesto findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Mesto> findAll() {
		return repository.findAll();
	}
}
