package jwd56.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd56.test.model.Rezervacija;
import jwd56.test.repository.RezervacijaRepository;
import jwd56.test.service.RezervacijaService;

@Service
public class JpaRezervacijaService implements RezervacijaService {
	
	@Autowired
	private RezervacijaRepository repository;

	@Override
	public Rezervacija findOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public List<Rezervacija> findAll() {
		return repository.findAll();
	}

	@Override
	public Rezervacija save(Rezervacija entitet) {
		return repository.save(entitet);
	}
}
