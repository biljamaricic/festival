package jwd56.test.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Mesto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String grad;
	
	@Column(length = 3)
	private String drzava;
	
	@Column(nullable = false)
	@OneToMany(mappedBy = "mesto")
	private List<Festival> festivali = new ArrayList<Festival>();

	public Mesto() {
		super();
	}

	public Mesto(Long id, String grad, String drzava, List<Festival> festivali) {
		super();
		this.id = id;
		this.grad = grad;
		this.drzava = drzava;
		this.festivali = festivali;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mesto other = (Mesto) obj;
		return Objects.equals(id, other.id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGrad() {
		return grad;
	}

	public void setGrad(String grad) {
		this.grad = grad;
	}

	public String getDrzava() {
		return drzava;
	}

	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}

	public List<Festival> getFestivali() {
		return festivali;
	}

	public void setFestivali(List<Festival> festivali) {
		this.festivali = festivali;
	}
	
	
}
