package jwd56.test.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd56.test.model.Festival;
import jwd56.test.model.Rezervacija;
import jwd56.test.service.FestivalService;
import jwd56.test.service.RezervacijaService;
import jwd56.test.support.FestivalDtoToFestival;
import jwd56.test.support.FestivalToFestivalDto;
import jwd56.test.support.RezervacijaDtoToRezervacija;
import jwd56.test.support.RezervacijaToRezervacijaDto;
import jwd56.test.web.dto.FestivalDTO;
import jwd56.test.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/festivali", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class FestivalController {
	
	@Autowired
	private FestivalDtoToFestival fromDto;

	@Autowired
	private FestivalToFestivalDto toDto;
	
	@Autowired
	private RezervacijaDtoToRezervacija toRezervacija;
	
	@Autowired
	private RezervacijaToRezervacijaDto toRezervacijaDto;
	
	@Autowired
	private RezervacijaService rezervacijaService;
	
	@Autowired
	private FestivalService service;
	
	
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FestivalDTO> create(@Valid @RequestBody FestivalDTO dto){
		Festival entitet = fromDto.convert(dto);
		Festival vrati = service.save(entitet);
		
		return new ResponseEntity<>(toDto.convert(vrati), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<FestivalDTO>> getAll(
			@RequestParam(required = false) String naziv,
			@RequestParam(required = false) Long mestoId,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo
			){
		
		Page<Festival> rezultati = service.search(naziv, mestoId, pageNo);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(rezultati.getTotalPages()));
		return new ResponseEntity<>(toDto.convert(rezultati.getContent()), headers, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<FestivalDTO> getOne(@PathVariable Long id){
		Festival entitet = service.findOne(id);
		
		if (entitet != null) {
			return new ResponseEntity<>(toDto.convert(entitet), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
		
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FestivalDTO> update(@PathVariable Long id, @Valid @RequestBody FestivalDTO dto){
		
		if (!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Festival entitet = fromDto.convert(dto);
		Festival vrati = service.update(entitet);
		
		return new ResponseEntity<>(toDto.convert(vrati), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		
		Festival obrisan = service.delete(id);
		
		if (obrisan != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/{id}/rezervacije")
	public ResponseEntity<RezervacijaDTO> create(@Valid @RequestBody RezervacijaDTO dto){
		Rezervacija entitet = toRezervacija.convert(dto);
		Rezervacija vrati = rezervacijaService.save(entitet);
		
		return new ResponseEntity<>(toRezervacijaDto.convert(vrati), HttpStatus.CREATED);
	}
	
    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
