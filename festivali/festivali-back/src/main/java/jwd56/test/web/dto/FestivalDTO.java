package jwd56.test.web.dto;

import java.time.LocalDate;

import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Length;

public class FestivalDTO {

	private Long id;
	
	@Length(max = 50)
	private String naziv;
	
	private LocalDate pocetak;
	
	private LocalDate kraj;
	
	@PositiveOrZero
	private Double cena;
	
	private Integer dostupneKarte;
	
	private Integer preostaleKarte;
	
	private Long mestoId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDate getPocetak() {
		return pocetak;
	}

	public void setPocetak(LocalDate pocetak) {
		this.pocetak = pocetak;
	}

	public LocalDate getKraj() {
		return kraj;
	}

	public void setKraj(LocalDate kraj) {
		this.kraj = kraj;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Integer getDostupneKarte() {
		return dostupneKarte;
	}

	public void setDostupneKarte(Integer dostupneKarte) {
		this.dostupneKarte = dostupneKarte;
	}

	public Integer getPreostaleKarte() {
		return preostaleKarte;
	}

	public void setPreostaleKarte(Integer preostaleKarte) {
		this.preostaleKarte = preostaleKarte;
	}

	public Long getMestoId() {
		return mestoId;
	}

	public void setMestoId(Long mestoId) {
		this.mestoId = mestoId;
	}
	
	
}
