package jwd56.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jwd56.test.model.Rezervacija;
import jwd56.test.service.RezervacijaService;
import jwd56.test.support.RezervacijaToRezervacijaDto;
import jwd56.test.web.dto.RezervacijaDTO;

@RestController
@RequestMapping(value = "/api/rezervacije", produces = MediaType.APPLICATION_JSON_VALUE)
public class RezervacijaController {

	@Autowired
	private RezervacijaToRezervacijaDto toDto;
	
	@Autowired
	private RezervacijaService service;
	
	@PreAuthorize("hasRole('ROLE_KORISNIK')")
	@GetMapping("/{id}")
	public ResponseEntity<RezervacijaDTO> getOne(@PathVariable Long id){
		Rezervacija entitet = service.findOne(id);
		
		if (entitet != null) {
			return new ResponseEntity<>(toDto.convert(entitet), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_KORISNIK')")
    @GetMapping
    public ResponseEntity<List<RezervacijaDTO>> get(){

        List<Rezervacija> rezultat = service.findAll();

        return new ResponseEntity<>(toDto.convert(rezultat), HttpStatus.OK);
    }

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
