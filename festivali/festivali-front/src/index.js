import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router, Routes, Route, Link, Navigate } from "react-router-dom";
import { Navbar, Container, NavbarBrand, Nav, NavLink, Button } from "react-bootstrap";
import { logout } from "./services/auth";
import Login from "./components/authorization/Login";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import RezervacijaAdd from "./components/Rezervacije/RezervacijaAdd";
import Festivali from "./components/Festivali/Festivali";
import FestivalAdd from "./components/Festivali/FestivalAdd";

class App extends React.Component {
    render(){
        const jwt = window.localStorage['jwt'];

        if(jwt) {
            return (<>
                <Router>
                    <Navbar expand bg="dark" variant="dark">
                        <NavbarBrand as={Link} to="/">
                            Medjunarodni<br/>festivali 
                        </NavbarBrand>
                        <Nav className="mr-auto">
                            <NavLink as={Link} to="/festivali">
                                Festivali
                            </NavLink>
                            <Button onClick={()=>logout()}>Logout</Button>
                        </Nav>
                    </Navbar>
                    <Container style={{paddingTop:"10px"}}>
                        <Routes>
                            <Route path="/" element={<Home/>}/>
                            <Route path="/login" element={<Navigate replace to="/"/>}/>
                            <Route path="/festivali" element={<Festivali/>}/>
                            <Route path="/festivali/add" element={<FestivalAdd/>}/>
                            <Route path="/festivali/:id/rezervacije/add" element={<RezervacijaAdd/>}/>
                            <Route path="*" element={<NotFound/>}/>
                        </Routes>
                    </Container>
                </Router>
            </>)
        } else {
            return(
                <>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <NavbarBrand as={Link} to="/">
                                Medjunarodni<br/>festivali
                            </NavbarBrand>
                            <Nav className="mr-auto">
                                <NavLink as={Link} to="/login">
                                    Login
                                </NavLink>
                            </Nav>
                        </Navbar>
                        <Container style={{paddingTop:"10px"}}>
                            <Routes>
                                <Route path="/" element={<Home/>}/>
                                <Route path="/login" element={<Login/>}/>
                                <Route path="*" element={<Navigate replace to="/login"/>}/>
                            </Routes>
                        </Container>
                    </Router>
                </>)
        }
    }
}

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);