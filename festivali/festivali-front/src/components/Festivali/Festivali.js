import React from "react";
import { Button, ButtonGroup, Form, FormControl, FormGroup, FormLabel, FormSelect, Table } from "react-bootstrap";
import TestAxios from "../../apis/TestAxios";
import { withNavigation, withParams } from "../../routeconf";
import RezervacijaModal from "../Rezervacije/RezervacijaModal";

class Festivali extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            modalFestivalId: -1,
            modalFestivalNaziv: "",
            showModal: false,
            showRender: false,
            festivali: [],
            mesta: [],
            search: { mestoId: -1, naziv: "" },
            pageNo: 0,
            totalPages: 1
        }
    }

    componentDidMount() {
        this.getData()
    }

    async getData() {
        await this.getFestivali(0);
        await this.getMesta();
    }

    async getFestivali(page) {

        let config = {
            params: {
                pageNo: page
            }
        }

        if (this.state.search.naziv != "") {
            config.params.naziv = this.state.search.naziv;
        }

        if (this.state.search.mestoId != -1) {
            config.params.mestoId = this.state.search.mestoId;
        }

        try {
            let res = await TestAxios.get("/festivali", config);
            if (res && res.status === 200) {

                this.setState({
                    pageNo: page,
                    totalPages: res.headers["total-pages"],
                    festivali: res.data
                })
            }
        } catch (err) {
            console.log(err)
            alert("Nije uspelo dobavljanje festivala.");
        }
    }

    async getMesta() {
        try {
            let res = await TestAxios.get("/mesta");
            if (res && res.status === 200) {
                this.setState({
                    mesta: res.data,
                    showRender: true
                })
            }
        } catch (error) {
            alert("Nije uspelo dobavljanje mesta.");
        }
    }

    searchValueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let search = this.state.search;
        search[name] = value;

        this.setState({ search: search });

        this.getFestivali(0);
    }

    async delete(festivalId) {
        try {
            let res = await TestAxios.delete("/festivali/" + festivalId);

            if (res && res.status === 204) {
                let nextPage;
                if (this.state.pageNo == this.state.totalPages - 1 && this.state.festivali.length == 1 && this.state.pageNo != 0) {
                    nextPage = this.state.pageNo - 1
                } else {
                    nextPage = this.state.pageNo
                }

                await this.getFestivali(nextPage);
            }
        } catch (error) {
            alert("Nije uspelo brisanje festivala.");
        }
    }

    goToAdd() {
        this.props.navigate("/festivali/add");
    }

    goToRezervacija(festivalId) {
        this.props.navigate("/festivali/" + festivalId + "/rezervacije/add")
    }

    showMesto(mestoId) {
        let mesto = this.state.mesta.filter(mes => mes.id == mestoId)

        return mesto[0].grad + ", " + mesto[0].drzava;
    }

    rezervacijaButtonRender(preostaleKarte, krajFestivala) {
        let danas = new Date();
        let kraj = new Date(krajFestivala);

        let vidljivo = false;

        if (kraj < danas) {
            vidljivo = true;
        }

        if (preostaleKarte === 0) {
            vidljivo = true;
        }

        return vidljivo;
    }

    async mestoInputChange(event, index) {
        let name = event.target.name;
        let value = event.target.value;

        let festivali = this.state.festivali;

        let festival = festivali[index];

        festival[name] = value;

        try {
            await TestAxios.put("/festivali/" + festival.id, festival);
            festivali.splice(index, 1, festival);
            this.setState({ festivali: festivali })
        } catch (error) {
            alert("Nije uspelo čuvanje promene mesta.");
        }
    }

    render() {
        if (this.state.showRender) {
            return <>

                <h1>Festivali</h1>
                <br />
                <br />

                <h3>Pretraga</h3>
                <Form>
                    <FormGroup>
                        <FormLabel>
                            Mesto odrzavanja
                        </FormLabel>
                        <FormSelect
                            onChange={(e) => this.searchValueInputChange(e)}
                            name="mestoId"
                            value={this.state.search.mestoId}
                        >
                            <option value={-1}></option>
                            {this.state.mesta.map((ent) => {
                                return (
                                    <option value={ent.id} key={ent.id}>
                                        {ent.grad + ", " + ent.drzava}
                                    </option>
                                )
                            })}
                        </FormSelect>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Naziv festivala
                        </FormLabel>
                        <FormControl
                            placeholder="Naziv festivala"
                            value={this.state.search.naziv}
                            name="naziv"
                            as="input"
                            onChange={(e) => this.searchValueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                </Form>

                {window.localStorage['role'] == "ROLE_ADMIN" ?
                    <Button variant="success" style={{ marginTop: 15, marginBottom: 4, float: "left" }} onClick={() => this.goToAdd()}>Kreiraj festival</Button>
                    : null}
                <ButtonGroup style={{ marginTop: 15, marginBottom: 3, float: "right" }}>

                    <Button
                        style={{ backgroundColor: "#39a2bd", border: "none" }}
                        disabled={this.state.pageNo == 0} onClick={() => this.getFestivali(this.state.pageNo - 1)}>
                        Prethodna
                    </Button>
                    <Button
                        style={{ backgroundColor: "#39a2bd", border: "none" }}
                        disabled={this.state.pageNo >= this.state.totalPages - 1} onClick={() => this.getFestivali(this.state.pageNo + 1)}>
                        Sledeca
                    </Button>
                </ButtonGroup>

                <Table striped hover style={{ marginTop: 5 }}>
                    <thead style={{ backgroundColor: "black", color: "white", border: "none" }}>
                        <tr>
                            <th>Naziv<br />festivala</th>
                            <th>Mesto odrzavanja</th>
                            <th>Datum pocetka<br />festivala</th>
                            <th>Datum zavrsetka<br />festivala</th>
                            <th>Cena karte<br />(RSD)</th>
                            <th>Broj preostalih<br />karata</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.festivali.map((festival, index) => {
                            return (
                                <tr key={festival.id}>
                                    <td>{festival.naziv}</td>
                                    <td>
                                        <FormSelect
                                            onChange={(e) => this.mestoInputChange(e, index)}
                                            name="mestoId"
                                            defaultValue={festival.mestoId}
                                        >
                                            <option value={-1}></option>
                                            {this.state.mesta.map((ent) => {
                                                return (
                                                    <option value={ent.id} key={ent.id}>
                                                        {ent.grad + ", " + ent.drzava}
                                                    </option>
                                                )
                                            })}
                                        </FormSelect>
                                    </td>
                                    <td>{festival.pocetak}</td>
                                    <td>{festival.kraj}</td>
                                    <td>{festival.cena}</td>
                                    <td>{festival.preostaleKarte}</td>

                                    {window.localStorage['role'] == "ROLE_ADMIN" ?
                                        <td>
                                            <Button onClick={() => this.delete(festival.id)} variant="danger">
                                                Obrisi
                                            </Button>
                                        </td>
                                        :
                                        // <td>
                                        //     <Button hidden={this.rezervacijaButtonRender(festival.preostaleKarte, festival.kraj)} onClick={() => this.goToRezervacija(festival.id)}>
                                        //         Rezervisi
                                        //     </Button>
                                        // </td>
                                        <td>
                                        <Button 
                                            hidden={this.rezervacijaButtonRender(festival.preostaleKarte, festival.kraj)} 
                                            onClick={() => this.setState({ showModal: true, modalFestivalId: festival.id, modalFestivalNaziv: festival.naziv })}
                                        >
                                            Rezervisi
                                        </Button>
                                    </td>
                                    }
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
                    
                <RezervacijaModal
                    show={this.state.showModal}
                    handleClose={() => this.setState({ showModal: false })}
                    festival={this.state.modalFestivalId}
                    festivalNaziv={this.state.modalFestivalNaziv}
                    reload={() => this.getFestivali()}
                />
            </>
        } else {
            return null;
        }
    }
}

export default withNavigation(withParams(Festivali));