import React from "react";
import { withNavigation } from "../../routeconf";
import TestAxios from "../../apis/TestAxios";
import { Button, Form, FormControl, FormGroup, FormSelect, FormLabel } from "react-bootstrap";

class FestivalAdd extends React.Component {

    constructor(props){
        super(props);

        let festival = {
            naziv: "",
            pocetak: "",
            kraj: "",
            cena: 0,
            dostupneKarte: 0,
            mestoId: -1
        }

        this.state = {  
            festival : festival,
            mesta : []         
        }
    }

    componentDidMount(){
        this.getMesta()
    }

    async getMesta(){
        try{
            let res = await TestAxios.get("/mesta");
            if (res && res.status === 200){
                this.setState({
                    mesta: res.data
                })
            }
        }catch (error) {
            alert("Nije uspelo dobavljanje mesta.");
        }
    }

    valueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let festival = this.state.festival;

        festival[name] = value;

        this.setState({ festival : festival });
    }

    async create(){
        try{
            let res = await TestAxios.post("/festivali", this.state.festival);
            if (res && res.status === 201){
                this.navigateFestivali();
            }
        } catch (err) {
            alert("Dodavanje festivala nije uspelo!");
        }
    }

    navigateFestivali(){
        this.props.navigate("/festivali");
    }

    render(){
            return(
                <Form>
                    <FormGroup>
                        <FormLabel>
                            Naziv festivala
                        </FormLabel>
                        <FormControl
                            placeholder="Naziv festivala"    
                            name = "naziv"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Datum pocetka festivala
                        </FormLabel>
                        <FormControl   
                            name = "pocetak"
                            as = "input"
                            type = "date"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Datum kraja festivala
                        </FormLabel>
                        <FormControl   
                            name = "kraj"
                            as = "input"
                            type = "date"
                            onChange = {(e) => this.valueInputChange(e)}
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Jedinicna cena karte
                        </FormLabel>
                        <FormControl
                            placeholder="Cena karte"    
                            name = "cena"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                            type = "number"
                            min = "0"
                            step = "0.1"
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Ukupan broj dostupnih karata
                        </FormLabel>
                        <FormControl
                            placeholder="Broj karata"    
                            name = "dostupneKarte"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                            type = "number"
                            min = "0"
                            step = "1"
                        ></FormControl>
                    </FormGroup>
                    <FormGroup>
                        <FormLabel>
                            Mesto odrzavanja
                        </FormLabel>
                        <FormSelect
                            onChange = {(e) => this.valueInputChange(e)}
                            name = "mestoId"
                            placeholder = "Izaberi mesto odrzavanja"
                        >
                            <option value={-1} style={{ color: "grey"}}>Izaberi mesto odrzavanja</option>
                            {this.state.mesta.map((sel) => {
                                return (
                                    <option value={sel.id} key={sel.id}>
                                        {sel.grad + ", " + sel.drzava}
                                    </option>
                                )
                            })}
                        </FormSelect>
                    </FormGroup>

                    <Button style={{marginTop: "30px"}} onClick={() => this.create()}>Kreiraj festival </Button>
                </Form>
            )
    }
}

export default withNavigation(FestivalAdd);