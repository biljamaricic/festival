import React from "react";
import { withNavigation, withParams } from "../../routeconf";
import TestAxios from "../../apis/TestAxios";
import { Button, FormControl, FormGroup, FormLabel, Modal, ModalBody, ModalFooter, ModalHeader, ModalTitle } from "react-bootstrap";

class RezervacijaModal extends React.Component {

    constructor(props) {
        super(props);

        let rezervacija = {
            brojKarata: 0,
            festivalId: this.props.festival
        }

        this.state = {
            rezervacija: rezervacija,
        }
    }

    valueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let rezervacija = this.state.rezervacija;

        rezervacija[name] = value;

        this.setState({ rezervacija: rezervacija });
    }

    async create() {
        let rezervacija = this.state.rezervacija;
        rezervacija.festivalId = this.props.festival

        try {
            let res = await TestAxios.post("/festivali/" + this.props.festival + "/rezervacije", rezervacija);
            if (res && res.status === 201) {
                alert("Rezervacija uspesno dodata!")
                this.props.handleClose();
                this.props.reload();
            }
        } catch (err) {
            alert("Dodavanje rezervacije nije uspelo!");
        }
    }

    navigateRezervacije() {
        this.props.navigate("/festivali");
    }

    render() {
        return (
            <Modal
                show={this.props.show}
                onHide={this.props.handleClose}
                backdrop="static"
            >
                <ModalHeader closeButton>
                    <ModalTitle>Rezervisi karte za {this.props.festivalNaziv}</ModalTitle>
                </ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <FormLabel>
                            Broj karata
                        </FormLabel>
                        <FormControl
                            placeholder="Unesite broj karata"
                            name="brojKarata"
                            as="input"
                            onChange={(e) => this.valueInputChange(e)}
                            type="number"
                            min="0"
                            step="1"
                        ></FormControl>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={() => this.create()}>Dodajte rezerevaciju</Button>
                </ModalFooter>
            </Modal>
        )
    }
}

export default withNavigation(withParams(RezervacijaModal));