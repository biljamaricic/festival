import React from "react";
import { withNavigation, withParams } from "../../routeconf";
import TestAxios from "../../apis/TestAxios";
import { Button, Form, FormControl, FormGroup, FormLabel } from "react-bootstrap";

class RezervacijaAdd extends React.Component {

    constructor(props){
        super(props);

        let rezervacija = {
            brojKarata: 0,
            festivalId: this.props.params.id
        }

        this.state = {  
            rezervacija : rezervacija,
        }
    }

    valueInputChange(event) {
        let name = event.target.name;
        let value = event.target.value;

        let rezervacija = this.state.rezervacija;
        
        rezervacija[name] = value;

        this.setState({ rezervacija : rezervacija });
    }

    async create(){
        try{
            let res = await TestAxios.post("/festivali/" + this.props.params.id + "/rezervacije", this.state.rezervacija);
            if (res && res.status === 201){
                alert("Rezervacija uspesno dodata!")
                this.navigateRezervacije();
            }
        } catch (err) {
            alert("Dodavanje rezervacije nije uspelo!");
        }
    }

    navigateRezervacije(){
        this.props.navigate("/festivali");
    }

    render(){
            return(
                <Form>
                    <FormGroup>
                        <FormLabel>
                            Broj karata
                        </FormLabel>
                        <FormControl
                            placeholder="Unesite broj karata"    
                            name = "brojKarata"
                            as = "input"
                            onChange = {(e) => this.valueInputChange(e)}
                            type = "number"
                            min = "0"
                            step = "1"
                        ></FormControl>
                    </FormGroup>

                    <Button style={{marginTop: "30px"}} onClick={() => this.create()}>Dodajte rezerevaciju</Button>
                </Form>
            )
    }
}

export default withNavigation(withParams(RezervacijaAdd));